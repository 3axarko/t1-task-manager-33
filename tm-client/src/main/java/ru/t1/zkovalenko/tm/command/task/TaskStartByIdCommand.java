package ru.t1.zkovalenko.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.dto.request.task.TaskStartByIdRequest;
import ru.t1.zkovalenko.tm.util.TerminalUtil;

public class TaskStartByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-start-by-id";

    @NotNull
    public static final String DESCRIPTION = "Task start by id";

    @Override
    public void execute() {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        TaskStartByIdRequest request = new TaskStartByIdRequest(getToken());
        request.setTaskId(id);
        getTaskEndpoint().startByIdTask(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
