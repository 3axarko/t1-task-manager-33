package ru.t1.zkovalenko.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.util.FormatUtil;

public class SystemInfoCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "info";

    @NotNull
    public static final String ARGUMENT = "-i";

    @NotNull
    public static final String DESCRIPTION = "Info about system";

    @Override
    public void execute() {
        System.out.println("Available processors (cores): " + Runtime.getRuntime().availableProcessors());

        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + FormatUtil.convertBytes(freeMemory));

        final long maxMemory = Runtime.getRuntime().maxMemory();
        @NotNull String maxMemoryChecked = maxMemory == Long.MAX_VALUE ? "no limit" : FormatUtil.convertBytes(maxMemory);
        System.out.println("Maximum memory: " + maxMemoryChecked);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory: " + FormatUtil.convertBytes(totalMemory));

        System.out.println("Usage memory: " + FormatUtil.convertBytes(totalMemory - freeMemory));
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
