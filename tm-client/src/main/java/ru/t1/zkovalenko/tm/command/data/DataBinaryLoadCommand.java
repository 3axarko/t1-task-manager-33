package ru.t1.zkovalenko.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.request.data.DataBinaryLoadRequest;
import ru.t1.zkovalenko.tm.enumerated.Role;

public final class DataBinaryLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-bin";

    @NotNull
    public static final String ARGUMENT = "-dlb";

    @NotNull
    public static final String DESCRIPTION = "Load data from binary file";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA BINARY LOAD]");
        @NotNull DataBinaryLoadRequest request = new DataBinaryLoadRequest(getToken());
        getDomainEndpoint().binaryLoadData(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
