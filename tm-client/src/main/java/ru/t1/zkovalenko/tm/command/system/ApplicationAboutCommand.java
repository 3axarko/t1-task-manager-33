package ru.t1.zkovalenko.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.dto.request.system.ServerAboutRequest;
import ru.t1.zkovalenko.tm.dto.response.system.ServerAboutResponse;

public class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "about";

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    public static final String DESCRIPTION = "Who did it? And why?";

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        @NotNull ServerAboutRequest request = new ServerAboutRequest();
        @NotNull ServerAboutResponse response = getSystemEndpoint().getAbout(request);
        System.out.println("Developer: " + response.getName());
        System.out.println("E-mail: " + response.getEmail());
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
