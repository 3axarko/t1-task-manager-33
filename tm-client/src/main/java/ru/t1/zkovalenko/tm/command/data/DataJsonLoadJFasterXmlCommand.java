package ru.t1.zkovalenko.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.request.data.DataJsonLoadJFasterXmlRequest;
import ru.t1.zkovalenko.tm.enumerated.Role;

public class DataJsonLoadJFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-json-fasterxml";

    @NotNull
    public static final String ARGUMENT = "-dljf";

    @NotNull
    public static final String DESCRIPTION = "Load data Json FasterXML from file";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA JSON FASTERXML LOAD]");
        @NotNull DataJsonLoadJFasterXmlRequest request = new DataJsonLoadJFasterXmlRequest(getToken());
        getDomainEndpoint().jsonLoadJFasterXmlData(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
