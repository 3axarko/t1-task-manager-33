package ru.t1.zkovalenko.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.request.data.DataJsonLoadJaxBRequest;
import ru.t1.zkovalenko.tm.enumerated.Role;

public class DataJsonLoadJaxBCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-json-jaxb";

    @NotNull
    public static final String ARGUMENT = "-dljj";

    @NotNull
    public static final String DESCRIPTION = "Load data json JaxB from file";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA JSON JAXB LOAD]");
        @NotNull DataJsonLoadJaxBRequest request = new DataJsonLoadJaxBRequest(getToken());
        getDomainEndpoint().jsonLoadJaxBData(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
