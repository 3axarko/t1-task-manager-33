package ru.t1.zkovalenko.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.dto.request.project.ProjectStartByIndexRequest;
import ru.t1.zkovalenko.tm.util.TerminalUtil;

public class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-start-by-index";

    @NotNull
    public static final String DESCRIPTION = "Project start by index";

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        ProjectStartByIndexRequest request = new ProjectStartByIndexRequest(getToken());
        request.setIndex(index);
        getProjectEndpoint().startByIndexProject(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
