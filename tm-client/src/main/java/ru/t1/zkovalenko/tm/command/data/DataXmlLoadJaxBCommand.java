package ru.t1.zkovalenko.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.request.data.DataXmlLoadJaxBRequest;
import ru.t1.zkovalenko.tm.enumerated.Role;

public class DataXmlLoadJaxBCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-xml-jaxb";

    @NotNull
    public static final String ARGUMENT = "-dlxj";

    @NotNull
    public static final String DESCRIPTION = "Load data xml JaxB from file";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA XML JAXB LOAD]");
        @NotNull DataXmlLoadJaxBRequest request = new DataXmlLoadJaxBRequest(getToken());
        getDomainEndpoint().xmlLoadJaxBData(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
