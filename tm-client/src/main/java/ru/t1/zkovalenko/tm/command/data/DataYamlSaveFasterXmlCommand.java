package ru.t1.zkovalenko.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.request.data.DataYamlSaveFasterXmlRequest;
import ru.t1.zkovalenko.tm.enumerated.Role;

public class DataYamlSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-yaml-fasterxml";

    @NotNull
    public static final String ARGUMENT = "-dsyf";

    @NotNull
    public static final String DESCRIPTION = "Save data Yaml FasterXML into file";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA YAML FASTERXML SAVE]");
        @NotNull DataYamlSaveFasterXmlRequest request = new DataYamlSaveFasterXmlRequest(getToken());
        getDomainEndpoint().yamlSaveFasterXmlData(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
