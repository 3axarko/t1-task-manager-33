package ru.t1.zkovalenko.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.request.user.UserRegistryRequest;
import ru.t1.zkovalenko.tm.enumerated.Role;
import ru.t1.zkovalenko.tm.model.User;
import ru.t1.zkovalenko.tm.util.TerminalUtil;

public class UserRegistryCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "user-registry";

    @NotNull
    private final String DESCRIPTION = "User registration";

    @Override
    public void execute() {
        System.out.println("[USER REGISTRATION]");
        System.out.println("ENTER LOGIN: ");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD: ");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL: ");
        @NotNull final String email = TerminalUtil.nextLine();
        UserRegistryRequest request = new UserRegistryRequest(getToken());
        request.setLogin(login);
        request.setPassword(password);
        request.setEmail(email);
        @NotNull final User user = getUserEndpoint().registryUser(request).getUser();
        System.out.println("[REGISTERED]");
        showUser(user);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
