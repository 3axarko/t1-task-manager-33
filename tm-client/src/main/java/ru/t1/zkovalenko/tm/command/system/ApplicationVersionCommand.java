package ru.t1.zkovalenko.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.dto.request.system.ServerVersionRequest;
import ru.t1.zkovalenko.tm.dto.response.system.ServerVersionResponse;

public class ApplicationVersionCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "version";

    @NotNull
    public static final String ARGUMENT = "-v";

    @NotNull
    public static final String DESCRIPTION = "Show app version";

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        @NotNull ServerVersionRequest request = new ServerVersionRequest();
        @NotNull final ServerVersionResponse response = getSystemEndpoint().getVersion(request);
        System.out.println(response.getVersion());
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
