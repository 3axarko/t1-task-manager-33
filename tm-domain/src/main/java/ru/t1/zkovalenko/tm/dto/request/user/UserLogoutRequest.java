package ru.t1.zkovalenko.tm.dto.request.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class UserLogoutRequest extends AbstractUserRequest {

    public UserLogoutRequest(@Nullable String token) {
        super(token);
    }

}
