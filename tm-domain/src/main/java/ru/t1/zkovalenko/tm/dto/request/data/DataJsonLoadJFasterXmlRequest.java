package ru.t1.zkovalenko.tm.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.request.user.AbstractUserRequest;

public class DataJsonLoadJFasterXmlRequest extends AbstractUserRequest {

    public DataJsonLoadJFasterXmlRequest(@Nullable String token) {
        super(token);
    }

}
