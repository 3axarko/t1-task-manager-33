package ru.t1.zkovalenko.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.request.user.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class TaskShowByProjectIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    public TaskShowByProjectIdRequest(@Nullable String token) {
        super(token);
    }

}
