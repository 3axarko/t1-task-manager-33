package ru.t1.zkovalenko.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.request.user.AbstractUserRequest;
import ru.t1.zkovalenko.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public class TaskChangeStatusByIdRequest extends AbstractUserRequest {

    @Nullable
    private String taskId;

    @Nullable
    private Status status;

    public TaskChangeStatusByIdRequest(@Nullable String token) {
        super(token);
    }

}
