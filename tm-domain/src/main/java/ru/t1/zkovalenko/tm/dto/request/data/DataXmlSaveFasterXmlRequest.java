package ru.t1.zkovalenko.tm.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.request.user.AbstractUserRequest;

public class DataXmlSaveFasterXmlRequest extends AbstractUserRequest {

    public DataXmlSaveFasterXmlRequest(@Nullable String token) {
        super(token);
    }

}
