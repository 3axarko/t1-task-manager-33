package ru.t1.zkovalenko.tm.dto.response.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.model.Task;

@NoArgsConstructor
public class TaskRemoveByIndexResponse extends AbstractTaskResponse {

    public TaskRemoveByIndexResponse(@Nullable final Task task) {
        super(task);
    }

}
