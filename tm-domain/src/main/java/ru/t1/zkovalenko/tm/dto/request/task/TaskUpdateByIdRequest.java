package ru.t1.zkovalenko.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.request.user.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class TaskUpdateByIdRequest extends AbstractUserRequest {

    @Nullable
    private String taskId;

    @Nullable
    private String name;

    @Nullable
    private String description;

    public TaskUpdateByIdRequest(@Nullable String token) {
        super(token);
    }

}
