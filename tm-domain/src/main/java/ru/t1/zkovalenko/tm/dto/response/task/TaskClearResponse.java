package ru.t1.zkovalenko.tm.dto.response.task;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class TaskClearResponse extends AbstractTaskResponse {
}
