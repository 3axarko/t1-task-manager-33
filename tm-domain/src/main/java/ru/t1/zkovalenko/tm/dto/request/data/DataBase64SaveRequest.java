package ru.t1.zkovalenko.tm.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.request.user.AbstractUserRequest;

public class DataBase64SaveRequest extends AbstractUserRequest {

    public DataBase64SaveRequest(@Nullable String token) {
        super(token);
    }

}
