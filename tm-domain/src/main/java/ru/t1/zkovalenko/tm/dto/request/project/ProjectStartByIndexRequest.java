package ru.t1.zkovalenko.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.request.user.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class ProjectStartByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    public ProjectStartByIndexRequest(@Nullable String token) {
        super(token);
    }

}
