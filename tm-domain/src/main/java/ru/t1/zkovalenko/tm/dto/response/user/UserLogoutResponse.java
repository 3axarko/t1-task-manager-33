package ru.t1.zkovalenko.tm.dto.response.user;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class UserLogoutResponse extends AbstractUserResponse {
}
