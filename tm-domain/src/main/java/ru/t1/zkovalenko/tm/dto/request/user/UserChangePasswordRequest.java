package ru.t1.zkovalenko.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class UserChangePasswordRequest extends AbstractUserRequest {

    @Nullable
    private String login;

    @Nullable
    private String password;

    public UserChangePasswordRequest(@Nullable String token) {
        super(token);
    }

}
