package ru.t1.zkovalenko.tm.dto.response.project;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ProjectRemoveByIndexResponse extends AbstractProjectResponse {
}
