package ru.t1.zkovalenko.tm.dto.request.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.request.user.AbstractUserRequest;

@NoArgsConstructor
public class TaskClearRequest extends AbstractUserRequest {

    public TaskClearRequest(@Nullable String token) {
        super(token);
    }

}
