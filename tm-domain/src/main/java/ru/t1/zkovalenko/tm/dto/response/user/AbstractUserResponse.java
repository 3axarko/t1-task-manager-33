package ru.t1.zkovalenko.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.dto.response.AbstractResponse;
import ru.t1.zkovalenko.tm.model.User;

@Getter
@Setter
@NoArgsConstructor
public class AbstractUserResponse extends AbstractResponse {

    @NotNull
    private User user;

    public AbstractUserResponse(@NotNull User user) {
        this.user = user;
    }

}
