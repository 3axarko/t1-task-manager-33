package ru.t1.zkovalenko.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.enumerated.Role;

@Getter
@Setter
public final class User extends AbstractModel {

    private static final long serialVersionUID = 1;

    @NotNull
    private String login;

    @NotNull
    private String passwordHash;

    @NotNull
    private String email = "";

    @NotNull
    private String firstName = "";

    @NotNull
    private String lastName = "";

    @NotNull
    private String middleName = "";

    @NotNull
    private Role role = Role.USUAL;

    @NotNull
    private Boolean locked = false;

}
