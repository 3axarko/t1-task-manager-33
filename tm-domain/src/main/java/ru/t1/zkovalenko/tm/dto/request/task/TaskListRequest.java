package ru.t1.zkovalenko.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.request.user.AbstractUserRequest;
import ru.t1.zkovalenko.tm.enumerated.Sort;

@Getter
@Setter
@NoArgsConstructor
public class TaskListRequest extends AbstractUserRequest {

    @Nullable
    private Sort sort;

    public TaskListRequest(@Nullable String token) {
        super(token);
    }

}
