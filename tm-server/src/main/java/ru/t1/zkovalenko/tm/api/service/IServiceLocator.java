package ru.t1.zkovalenko.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IServiceLocator {

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    ISessionService getSessionService();

}
