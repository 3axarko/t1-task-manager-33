package ru.t1.zkovalenko.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.api.repository.ISessionRepository;
import ru.t1.zkovalenko.tm.api.service.ISessionService;
import ru.t1.zkovalenko.tm.model.Session;

public class SessionService extends AbstractUserOwnerService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull ISessionRepository repository) {
        super(repository);
    }

}
